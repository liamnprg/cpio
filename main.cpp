#include <fstream>
#include <iostream>
#include <string>
#include "LibCpio.h"

using namespace std;

//test driver: create three cpio archives of the three different header formats from a directory using gnu cpio
//extract the three cpio archives
//compare directories
//create a cpio archive using my program
//unpack it using gnu cpio
//compare directories

void print_buf(vector<char> buf) {
    for (char c: buf) {
        cout << c;
    }
}

void print_hdr(HeaderFile *hdr) {
    hdr->display();
    cout << hdr->get_file_name() << '\n';
    cout << "\n\n\n";
    vector<char> buf = hdr->read_nth_block_of_file(0);
    print_buf(buf);
    int i = 1;
    //displays the whole file
    while (/*i<23*/!buf.empty()) {
        buf = hdr->read_nth_block_of_file(i);
        print_buf(buf);
        i++;
    }
    cout << "correct EOF" << hdr->get_stream_pos() << endl;
    cout << "reported EOF" << hdr->get_end_of_headerfile() << endl;
}

int main() {
    //const int magic = 0x71c7;
    //char str[10];

    ifstream cpio_archive_f;
    cpio_archive_f.open("../test_archives/LibCpio_odc.cpio", ios::in | ios::binary); 

    istream &archive = cpio_archive_f;
    cpio_archive *ar = new cpio_archive;
    ar->extract_archive(&archive,"./");

    /*
    HeaderFile hdr(&cpio_archive_f,0);

    print_hdr(&hdr);


    unsigned long n=hdr.get_end_of_headerfile();
    HeaderFile trailer(&cpio_archive_f,n);

    print_hdr(&trailer);
    */
}



