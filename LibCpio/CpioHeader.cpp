#include "LibCpio.h"
#include <cstring>
#include <iostream>
#include <istream>
#include <sstream>
#include <string>
#include <sys/stat.h>
#include <vector>

using std::cout;
using std::istream;
using std::stoi;
using std::string;
using std::stringstream;
using std::vector;

//octal format
internal_header parse_oldc_header(istream* stream, unsigned long loc)
{
    stream->seekg(loc);
    cpio_odc_header Hdr;
    memset(&Hdr, 0, sizeof(cpio_odc_header));
    stream->read(Hdr.c_magic, sizeof(char) * 6);
    stream->read(Hdr.c_dev, sizeof(char) * 6);
    stream->read(Hdr.c_ino, sizeof(char) * 6);
    stream->read(Hdr.c_mode, sizeof(char) * 6);
    stream->read(Hdr.c_uid, sizeof(char) * 6);
    stream->read(Hdr.c_gid, sizeof(char) * 6);
    stream->read(Hdr.c_nlink, sizeof(char) * 6);
    stream->read(Hdr.c_rdev, sizeof(char) * 6);
    stream->read(Hdr.c_mtime, sizeof(char) * 11);
    stream->read(Hdr.c_namesize, sizeof(char) * 6);
    stream->read(Hdr.c_filesize, sizeof(char) * 11);

    internal_header i_hdr;
    memset(&i_hdr, 0, sizeof(internal_header));
    i_hdr.c_magic = stoi(Hdr.c_magic, nullptr, 8);
    i_hdr.c_dev = stoi(Hdr.c_dev, nullptr, 8);
    i_hdr.c_ino = stoi(Hdr.c_ino, nullptr, 8);
    i_hdr.c_mode = stoi(Hdr.c_mode, nullptr, 8);
    i_hdr.c_uid = stoi(Hdr.c_uid, nullptr, 8);
    i_hdr.c_gid = stoi(Hdr.c_gid, nullptr, 8);
    i_hdr.c_nlink = stoi(Hdr.c_nlink, nullptr, 8);
    i_hdr.c_rdev = stoi(Hdr.c_rdev, nullptr, 8);
    i_hdr.c_mtime = stoi(Hdr.c_mtime, nullptr, 8);
    //oldc header has smaller name(no comma after name?)
    i_hdr.c_namesize = stoi(Hdr.c_namesize, nullptr, 8);
    i_hdr.c_namesize--;
    i_hdr.c_filesize = stoi(Hdr.c_filesize, nullptr, 8);
    return i_hdr;
}

internal_header parse_newc_header(istream* stream, unsigned long loc)
{
    new_ascii_header hdr;
    memset(&hdr, 0, sizeof(new_ascii_header));
    stream->seekg(loc);

    stream->read(hdr.c_magic, sizeof(char) * 6);
    stream->read(hdr.c_ino, sizeof(char) * 8);
    stream->read(hdr.c_mode, sizeof(char) * 8);
    stream->read(hdr.c_uid, sizeof(char) * 8);
    stream->read(hdr.c_gid, sizeof(char) * 8);
    stream->read(hdr.c_nlink, sizeof(char) * 8);
    stream->read(hdr.c_mtime, sizeof(char) * 8);
    stream->read(hdr.c_filesize, sizeof(char) * 8);
    stream->read(hdr.c_dev_maj, sizeof(char) * 8);
    stream->read(hdr.c_dev_min, sizeof(char) * 8);
    stream->read(hdr.c_rdev_maj, sizeof(char) * 8);
    stream->read(hdr.c_rdev_min, sizeof(char) * 8);
    stream->read(hdr.c_namesize, sizeof(char) * 8);
    stream->read(hdr.c_chksum, sizeof(char) * 8);

    internal_header i_hdr;
    memset(&i_hdr, 0, sizeof(internal_header));

    //these are in hex but in ascii lololol
    //"000003E8"
    i_hdr.c_magic = stoi(hdr.c_magic, nullptr, 16);
    i_hdr.c_nlink = stoi(hdr.c_nlink, nullptr, 16);
    //don't care
    i_hdr.c_dev = 0;
    i_hdr.c_ino = stoi(hdr.c_ino, nullptr, 16);
    i_hdr.c_mode = stoi(hdr.c_mode, nullptr, 16);
    i_hdr.c_uid = stoi(hdr.c_uid, nullptr, 16);
    i_hdr.c_gid = stoi(hdr.c_gid, nullptr, 16);
    i_hdr.c_nlink = stoi(hdr.c_nlink, nullptr, 16);
    //don't care
    i_hdr.c_rdev = 0;
    i_hdr.c_mtime = stoi(hdr.c_mtime, nullptr, 16);
    i_hdr.c_namesize = stoi(hdr.c_namesize, nullptr, 16);
    i_hdr.c_filesize = stoi(hdr.c_filesize, nullptr, 16);
    return i_hdr;
}

internal_header convert_bin_header_to_internal_header(struct old_cpio_header* Bin)
{
    internal_header hdr;
    hdr.c_magic = Bin->c_magic;
    hdr.c_ino = Bin->c_ino;
    hdr.c_mode = Bin->c_mode;
    hdr.c_uid = Bin->c_uid;
    hdr.c_gid = Bin->c_gid;
    hdr.c_nlink = Bin->c_nlink;
    unsigned long mtime = (unsigned long)Bin->c_mtimes[0] << 16 | Bin->c_mtimes[1];
    hdr.c_mtime = mtime;
    unsigned long filesize = (unsigned long)Bin->c_filesizes[0] << 16 | Bin->c_filesizes[1];
    hdr.c_filesize = filesize;
    hdr.c_dev = Bin->c_dev;
    hdr.c_rdev = Bin->c_rdev;
    hdr.c_namesize = Bin->c_namesize;
    return hdr;
}

struct stat stat_string(string fname)
{
    struct stat s;
    stat(fname.data(), &s);
    return s;
}

internal_header odc_header_from_path(string path)
{
    struct stat s = stat_string(path);
    internal_header hdr;
    memset(&hdr, 0, sizeof(internal_header));
    hdr.c_magic = 29127;
    hdr.c_dev = s.st_dev;
    hdr.c_ino = s.st_ino;
    hdr.c_mode = s.st_mode;
    hdr.c_uid = s.st_uid;
    hdr.c_gid = s.st_gid;
    hdr.c_nlink = s.st_nlink;
    hdr.c_rdev = s.st_rdev;
    hdr.c_mtime = 0;
    //null byte
    hdr.c_namesize = path.size() + 1;
    hdr.c_filesize = s.st_size;
    return hdr;
}

void writeoct(char* buf, unsigned long ot, int n)
{
    while (ot != 0) {
        buf[n - 1] = ((ot % 8)) + 0x30;
        ot = ot / 8;
        n--;
    }
    return;
}
string serialize_header_to_odc(struct internal_header h)
{
    cpio_odc_header odc;
    //'0'
    memset(&odc, 0x30, sizeof(cpio_odc_header));

    writeoct(odc.c_magic, h.c_magic, 6);
    writeoct(odc.c_dev, h.c_dev, 6);
    writeoct(odc.c_ino, h.c_ino, 6);
    writeoct(odc.c_mode, h.c_mode, 6);
    writeoct(odc.c_uid, h.c_uid, 6);
    writeoct(odc.c_gid, h.c_gid, 6);
    writeoct(odc.c_nlink, h.c_nlink, 6);
    writeoct(odc.c_rdev, h.c_rdev, 6);
    writeoct(odc.c_mtime, h.c_mtime, 11);
    writeoct(odc.c_namesize, h.c_namesize, 6);
    writeoct(odc.c_filesize, h.c_filesize, 11);
    string ss(sizeof(cpio_odc_header), 0x0);
    memcpy((void*)ss.data(), &odc, sizeof(cpio_odc_header));

    /*cout << odc.c_magic;
    cout << odc.c_dev;
    cout << odc.c_ino;
    cout << odc.c_mode;
    cout << odc.c_uid;
    cout << odc.c_gid;
    cout << odc.c_nlink;
    cout << odc.c_rdev;
    cout << odc.c_mtime;
    cout << odc.c_namesize;
    cout << odc.c_filesize;*/

    return ss;
}
