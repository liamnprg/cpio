#include "LibCpio.h"
#include <cstring>
#include <fstream>
#include <iostream>
#include <istream>
#include <string>
#include <sys/stat.h>

using std::cout;
using std::endl;
using std::ifstream;
using std::istream;
using std::string;
using std::vector;

//move to new class
HeaderFile::HeaderFile(string path)
    : s_hdr(odc_header_from_path(path))
    , s_fname(path)
    , s_htype(Intl)
{
    ifstream* f = new ifstream;
    f->open(path.data(), std::ios::in | std::ios::binary);
    m_stream = f;
    m_stream->seekg(0);
}
HeaderFile::~HeaderFile()
{
    delete (m_stream);
}

string HeaderFile::serialize_header()
{
    return serialize_header_to_odc(s_hdr);
}

HeaderFile::HeaderFile(istream* Stream, unsigned long loc)
    : m_stream(Stream)
    , s_loc(loc)
{
    old_cpio_header* Bin = NULL;
    m_stream->seekg(s_loc);

    //headers are not null-terminated
    //read new ascii header
    char Newc_header[6];
    m_stream->read(Newc_header, 6 * sizeof(char));
    m_stream->seekg(s_loc);

    //read Odc header
    char Odc_header[6];
    m_stream->read(Odc_header, 6 * sizeof(char));
    m_stream->seekg(s_loc);

    /*char c = Newc_header[5];
    Newc_header[5]='\0';
    cout << Newc_header<<c;*/
    //last attempt -- binary header
    //fix this crap
    char* buffer = new char[sizeof(old_cpio_header)];
    m_stream->read(buffer, sizeof(old_cpio_header));
    Bin = reinterpret_cast<old_cpio_header*>(buffer);

    headertype Htype = headertype::Bin;
    internal_header i_hdr;
    if (strncmp(Odc_header, "070707", 6) == 0) {
        Htype = Oldc;
        s_htype = Htype;
        //cout << "Odc" << endl;
        i_hdr = parse_oldc_header(m_stream, s_loc);
    } else if (strncmp(Newc_header, "070701", 6) == 0) {
        Htype = Newc;
        s_htype = Htype;
        //cout << "Newc" << endl;
        i_hdr = parse_newc_header(m_stream, s_loc);
    } else if (Bin->c_magic == 29127) {
        Htype = headertype::Bin;
        s_htype = Htype;
        i_hdr = convert_bin_header_to_internal_header(Bin);
    } else {
        cout << "bin_header: " << Bin->c_magic << endl;
        cout << "Unrecognized\n";
        exit(1);
    }
    delete (buffer);
    s_hdr = i_hdr;
    s_end_of_header = m_stream->tellg();
    get_file_name();
    //cout << "Filename: " << get_file_name() << endl;
    //moves stream pointer to multiple of 4 as described by the holy man 5 cpio
    //rewrite for efficency>clarity maybe
    //cout << "start_of_file_b: " << m_stream->tellg() << '\n';
    if (s_htype == Newc) {
        while (m_stream->tellg() % 4 != 0)
            m_stream->seekg(1, std::ios_base::cur);
    }
    //man 5 cpio
    if (s_htype == headertype::Bin && Bin->c_namesize % 2 == 1) {
        m_stream->seekg(1, std::ios_base::cur);
    }
    //cout << "start_of_file_a: " << m_stream->tellg() << '\n';
    s_start_of_file = m_stream->tellg();
}
string HeaderFile::get_file_name()
{
    if (s_htype == Intl) {
        return s_fname;
    }
    char* filename = new char[s_hdr.c_filesize];
    //cout << "End_of_header: " << end_of_header << '\n';
    //cout << "c_namesize" << hdr.c_namesize << endl;
    m_stream->seekg(s_end_of_header);
    if (s_htype == Bin || s_htype == Newc) {
        m_stream->read(filename, sizeof(char) * s_hdr.c_namesize);
    } else {
        m_stream->read(filename, sizeof(char) * s_hdr.c_namesize + 1);
    }
    string s_filename(filename);
    delete (filename);
    return s_filename;
}
void HeaderFile::display()
{
    cout << "c_magic: " << s_hdr.c_magic << endl;
    cout << "c_dev: " << s_hdr.c_dev << endl;
    cout << "c_ino: " << s_hdr.c_ino << endl;
    cout << "c_mode: " << s_hdr.c_mode << endl;
    cout << "c_uid: " << s_hdr.c_uid << endl;
    cout << "c_gid: " << s_hdr.c_gid << endl;
    cout << "c_nlink: " << s_hdr.c_nlink << endl;
    cout << "c_rdev: " << s_hdr.c_rdev << endl;
    cout << "c_mtime: " << s_hdr.c_mtime << endl;
    cout << "c_namesize: " << s_hdr.c_namesize << endl;
    cout << "c_filesize: " << s_hdr.c_filesize << endl;
    cout << "end_of_header: " << s_end_of_header << endl;
    cout << "s_start_of_file: " << s_start_of_file << endl;
}
//this gets the location of the end of this header and associated file, the location is the offset of the next headerfile combination
unsigned long HeaderFile::get_end_of_headerfile()
{
    unsigned long n = 0;
    n += s_start_of_file;
    n += s_hdr.c_filesize;
    if (s_htype == Newc) {
        //newc has two null bytes at the end of each headerfile
        //0x0A gets missed at the end of each file
        //n+=1;
        while (n % 4 != 0)
            n++;
        //man 5 cpio
    } else if (s_htype == Bin && n % 2 == 1) {
        n += 1;
    }
    //cout << start_of_file+hdr.c_filesize << " p:a " << n << endl;
    return n;
}
unsigned long HeaderFile::get_stream_pos()
{
    return m_stream->tellg();
}
vector<char> HeaderFile::read_nth_block_of_file(unsigned long n)
{
    unsigned long filesize = s_hdr.c_filesize;

    n = n * 512;
    //we sub 512 since n is where we start reading instead of where we stop
    //this case is where the start of our reading is greater than the end of the file
    unsigned long size = 0;

    if (filesize < n) {
        vector<char> buf(0);
        return buf;
        //this case is where we can't read 512 bytes but we could read (for example) 384
    } else if (filesize - n < 512) {
        size = filesize - n;
        //more than 512 blocks left to read
    } else {
        size = 512;
    }
    vector<char> buf(size);
    seek_file_offset(n);
    if (m_stream->read(buf.data(), sizeof(char) * size).good()) {
        return buf;
    } else {
        cout << "sad exit" << '\n';
        exit(111);
    }
}

void HeaderFile::seek_file_offset(unsigned long n)
{
    m_stream->seekg(s_start_of_file + n);
    return;
}

bool HeaderFile::is_dir()
{
    unsigned long p = (s_hdr.c_mode & 0170000);
    return p == 0040000;
}

//rename lol
int HeaderFile::get_rwx()
{
    //rwx perms, includes sticky bit and setuid,setgid
    int mask = 0007777;
    int rwx = s_hdr.c_mode & mask;
    return rwx;
}
