#include "LibCpio.h"
#include <cstring>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <istream>
#include <string>
#include <sys/stat.h>

using std::cout;
using std::endl;
using std::istream;
using std::ostream;
using std::string;
using std::vector;

void write_archive_file_to_file(string fname, HeaderFile hdr);
void cpio_archive::list_files(istream* archive)
{
    unsigned long loc = 0;
    HeaderFile hdr(archive, loc);
    string file_name = hdr.get_file_name();
    string trailer("TRAILER!!!");
    while (file_name.compare(trailer)) {
        cout << file_name << endl;
        unsigned long eohf = hdr.get_end_of_headerfile();
        hdr = HeaderFile(archive, eohf);
        file_name = hdr.get_file_name();
    }
    return;
}

void print_buf_to(vector<char> buf, ostream* archive_dest)
{
    for (char c : buf) {
        *archive_dest << c;
    }
}

void append_file(string path, ostream* archive_dest)
{
    HeaderFile hdr(path);
    *archive_dest << hdr.serialize_header();
    *archive_dest << hdr.get_file_name();
    *archive_dest << '\0';
    vector<char> buf = hdr.read_nth_block_of_file(0);
    print_buf_to(buf, archive_dest);
    int i = 1;
    //displays the whole file
    while (/*i<23*/ !buf.empty()) {
        buf = hdr.read_nth_block_of_file(i);
        print_buf_to(buf, archive_dest);
        i++;
    }
    i = 0;
    //while (i < 0xf7-40) {
}

void cpio_archive::create_archive(istream* list_of_files, ostream* archive_dest)
{
    string s;
    char c = '\0';
    while (list_of_files->get(c)) {
        if (c == '\n' || c == ' ') {
            append_file(s, archive_dest);
            s = "";
        } else {
            s += c;
        }
    }
    *archive_dest << "0707070000000000000000000000000000000000010000000000000000000001300000000000TRAILER!!!";
    int i = 1;
    while (i < 10) {
        *archive_dest << '\0';
        i++;
    }
    return;
}

void cpio_archive::extract_archive(istream* archive, string prefix)
{
    unsigned long loc = 0;
    HeaderFile hdr(archive, loc);
    string file_name = hdr.get_file_name();
    string trailer("TRAILER!!!");
    while (file_name.compare(trailer)) {
        file_name = prefix + file_name;
        cout << file_name << endl;

        write_archive_file_to_file(file_name, hdr);
        //post
        unsigned long eohf = hdr.get_end_of_headerfile();
        hdr = HeaderFile(archive, eohf);
        file_name = hdr.get_file_name();
    }
    return;
}

//note: assuming no cpio archives containing sockets, symbolic links, block devices, character devices, pipes or fifo's
void write_archive_file_to_file(string fname, HeaderFile hdr)
{
    if (hdr.is_dir()) {
        //cout << "MKDIR\n";
        mkdir(fname.data(), hdr.get_rwx());
        return;
    }
    std::ofstream outf { fname };
    vector<char> buf = hdr.read_nth_block_of_file(0);
    unsigned long n = 1;
    while (!buf.empty()) {
        outf.write(&buf[0], buf.size());
        ;
        buf = hdr.read_nth_block_of_file(n);
        n++;
    }
    chmod(fname.data(), hdr.get_rwx());
    outf.close();
}
