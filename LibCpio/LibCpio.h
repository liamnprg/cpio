#pragma once
#include <string>
#include <vector>
//types of headers
enum headertype {
    Oldc,
    Newc,
    //this is the type for a header created using the path constructor
    Intl,
    Bin
};

/*-------cpio header structs-------*/

//"Lifted" from GNU CPIO v 2.13 sourced via gnu.org, the defualt cpio archive format
//only applies to cpio header definitions: old_cpio_header,cpio odc header, new ascii header
//seems to be gnu cpio's defualt header
#pragma pack(1)
struct old_cpio_header {
    unsigned short c_magic;
    unsigned short c_dev;
    unsigned short c_ino;
    unsigned short c_mode;
    unsigned short c_uid;
    unsigned short c_gid;
    unsigned short c_nlink;
    unsigned short c_rdev;
    unsigned short c_mtimes[2];
    unsigned short c_namesize;
    unsigned short c_filesizes[2];
};

//cpio man 5 page
//this is the header for the -c cpio flag(ref. man cpio
//aka portable ascii header
#pragma pack(1)
struct cpio_odc_header {
    char c_magic[6];
    char c_dev[6];
    char c_ino[6];
    char c_mode[6];
    char c_uid[6];
    char c_gid[6];
    char c_nlink[6];
    char c_rdev[6];
    char c_mtime[11];
    char c_namesize[6];
    char c_filesize[11];
};

//default cpio archive format is bin
#pragma pack(1)
struct new_ascii_header {
    char c_magic[7]; /* "070701" for "new" portable format
                            "070702" for CRC format */
    char c_ino[9];
    char c_mode[9];
    char c_uid[9];
    char c_gid[9];
    char c_nlink[9];
    char c_mtime[9];
    char c_filesize[9]; /* must be 0 for FIFOs and directories */
    char c_dev_maj[9];
    char c_dev_min[9];
    char c_rdev_maj[9]; /* only valid for chr and blk special files */
    char c_rdev_min[9]; /* only valid for chr and blk special files */
    char c_namesize[9]; /* count includes terminating NUL in pathname */
    char c_chksum[9];   /* 0 for "new" portable format; for CRC format
                            the sum of all the bytes in the file  */
};

//we will take input headers and transform them into this, then output them as another header

struct internal_header {
    unsigned long c_magic;
    unsigned long c_dev;
    unsigned long c_ino;
    unsigned long c_mode;
    unsigned long c_uid;
    unsigned long c_gid;
    unsigned long c_nlink;
    unsigned long c_rdev;
    unsigned long c_mtime;
    unsigned long c_namesize;
    unsigned long c_filesize;
};

/*-------Represents one File and Header combination -- often referred to as a fileheader-------*/

class HeaderFile {
private:
    struct internal_header s_hdr;
    std::istream* m_stream { nullptr };
    //end of header=start of fileNAME
    unsigned long s_end_of_header { 0 };
    unsigned long s_start_of_file { 0 };
    //fname cached when importing header from path
    std::string s_fname;
    //location of the header in the stream
    unsigned long s_loc { 0 };
    headertype s_htype;

public:
    void seek_file_offset(unsigned long n);
    //return header at loc
    HeaderFile(std::istream* Stream, unsigned long loc);
    //create header from file located at path
    HeaderFile(std::string path);
    ~HeaderFile();
    void display();
    std::string get_file_name();
    unsigned long get_stream_pos();
    unsigned long get_end_of_headerfile();
    std::string serialize_header();
    std::vector<char> read_nth_block_of_file(unsigned long n);
    bool is_dir();
    int get_rwx();
};

/*-------Represents a transient dd-like file concaternation and deconcaternation library-------*/
class cpio_archive {
public:
    //creates a list of files from list_of_files and writes the archive at archive_dest
    void create_archive(std::istream* list_of_files, std::ostream* archive_dest);
    //extracts archive, optionally with prefix prefix
    void extract_archive(std::istream* archive, std::string prefix);
    //prints files in archive to stdout
    void list_files(std::istream* archive);
};

/*-------Simple utilities to read raw headers to a streamlined internal_header definition-------*/

internal_header convert_bin_header_to_internal_header(old_cpio_header*);
internal_header parse_oldc_header(std::istream*, unsigned long);
internal_header parse_newc_header(std::istream*, unsigned long);
internal_header odc_header_from_path(std::string path);
std::string serialize_header_to_odc(struct internal_header h);
